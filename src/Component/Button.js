import React from 'react';
import Input from './Input';
import '../Custom.css'
class Button extends React.Component
{
    buttonValue=()=>
    {
        
        if(this.props.label==="=")
        {
           var a=document.getElementById("Input").value;
                if(a[0]==="+"||a[0]==="-"||a[0]==="*"||a[0]==="/")
                {
                    alert("Not allowed..");
                    document.getElementById("Input").value="";
                }
                else
                {
                    
                    try
                    {
                    document.getElementById("Input").value=eval(a);
                    document.getElementById("ans").innerHTML=a+" = ";
                    }
                    catch (e) {
                        if (e instanceof SyntaxError) {
                            alert(e.message);
                            document.getElementById("Input").value="";
                            document.getElementById("ans").innerHTML="Error";
                        }
                    }
                }
            
        }
        else if(this.props.label==="C")
        {
            document.getElementById("Input").value="";
        }
        else if(this.props.label===<i class='fas fa-backspace'></i>)
        {
            let a= document.getElementById("Input").value
            document.getElementById("txt1").value=a.substring(0, a.length - 1);
        }
        else
        {
            let a=document.getElementById("Input").value;
            if(a==="")
            {
                document.getElementById("Input").value=this.props.label;
                
            }
            else
            {
                document.getElementById("Input").value+=this.props.label;
            }
        }

    }

    render()
    {
        return(
            <div className="keys">
            <button  className="button gray"  onClick={this.buttonValue}>{this.props.label}</button>
            </div>
        )
    }
}
export default Button;